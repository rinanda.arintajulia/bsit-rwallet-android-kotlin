package com.bcas.rwallet

import java.time.temporal.TemporalAmount

class Transaction(
    val name: String,
    val status: String,
    val amount: String,
    val imageUrl: String
)