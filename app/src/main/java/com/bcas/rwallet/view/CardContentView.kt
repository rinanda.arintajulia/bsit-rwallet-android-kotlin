package com.bcas.rwallet.view

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import com.bcas.rwallet.R
import com.bumptech.glide.Glide

class CardContentView(context: Context, attributeSet: AttributeSet) :
    FrameLayout(context, attributeSet) {

    private val ccImageView: AppCompatImageView by lazy {
        findViewById(R.id.cc_image)
    }

    private val ccTvTitle: TextView by lazy {
        findViewById(R.id.cc_tv_title)
    }

    private val ccTvSubtitle: TextView by lazy {
        findViewById(R.id.cc_tv_subtitle)
    }

    private val ccTvPrice: TextView by lazy {
        findViewById(R.id.cc_tv_price)
    }

    // init block
    // ini kode yg di eksekusi ketika instance dari kelas ini
    // dibuat.
    init {
            inflate(context, R.layout.layout_card_content_view, this)
    }

    fun setImageUrl(imageUrl: String) {
        Glide.with(this).load(imageUrl).into(ccImageView)

    }

    fun setTitle(title: String) {
        ccTvTitle.text = title
    }

    fun setSubtitle(subtitle: String) {
        ccTvSubtitle.text = subtitle
    }

    fun setPrice(price: String) {
        ccTvPrice.text = price
    }
}