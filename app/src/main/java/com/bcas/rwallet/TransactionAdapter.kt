package com.bcas.rwallet

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bcas.rwallet.databinding.ItemMainBinding

class TransactionAdapter : RecyclerView.Adapter<TransactionAdapter.TransactionViewHolder>() {

    inner class TransactionViewHolder(
        private val binding: ItemMainBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(transaction: Transaction) {
            binding.itemCc.setImageUrl(transaction.imageUrl)
            binding.itemCc.setTitle(transaction.name)
            binding.itemCc.setSubtitle(transaction.status)
            binding.itemCc.setPrice(transaction.amount)
        }
    }

    private val items: MutableList<Transaction> = mutableListOf()

    fun addNewItems(newItems: List<Transaction>) {
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding =  ItemMainBinding.inflate(inflater, parent, false)
        return TransactionViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return  items.size
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item)
    }
}