package com.bcas.rwallet

object DataSources {
    val transactionList = listOf(
        Transaction("Pembayaran listrik", "Berhasil", "Rp 250.000", "https://cdn.pixabay.com/photo/2017/03/16/18/36/summer-2149911__480.jpg"),
        Transaction("Pembayaran telepon", "Gagal", "Rp 100.000", "https://cdn.pixabay.com/photo/2017/04/06/19/16/cat-2209109__480.jpg"),
        Transaction("Pembelian makanan", "Berhasil", "Rp 150.000", "https://cdn.pixabay.com/photo/2017/04/05/10/31/cat-baby-2204590__480.jpg"),
        Transaction("Pembayaran tagihan air", "Gagal", "Rp 200.000", "https://cdn.pixabay.com/photo/2017/02/24/17/10/chartreux-2095661__480.jpg"),
        Transaction("Pembelian tiket bioskop", "Berhasil", "Rp 75.000", "https://cdn.pixabay.com/photo/2017/05/13/07/18/cat-2308849__480.jpg"),
        Transaction("Pembayaran tagihan internet", "Gagal", "Rp 300.000", "https://cdn.pixabay.com/photo/2017/05/18/11/21/cat-2323326__480.jpg"),
        Transaction("Pembelian baju baru", "Berhasil", "Rp 500.000", "https://cdn.pixabay.com/photo/2017/03/06/09/16/cat-2120915__480.jpg"),
        Transaction("Pembayaran cicilan mobil", "Berhasil", "Rp 5.000.000", "https://cdn.pixabay.com/photo/2017/05/23/09/01/cat-2336605__480.jpg"),
        Transaction("Pembelian kamera", "Gagal", "Rp 7.500.000", "https://cdn.pixabay.com/photo/2016/11/21/12/52/animal-1845248__480.jpg"),
        Transaction("Pembayaran tagihan listrik", "Berhasil", "Rp 150.000", "https://cdn.pixabay.com/photo/2017/04/07/18/37/cat-2211609__480.jpg"),
        Transaction("Pembelian sepatu", "Gagal", "Rp 800.000", "https://cdn.pixabay.com/photo/2016/03/09/15/27/cat-1246659__480.jpg"),
        Transaction("Pembayaran tagihan telepon", "Berhasil", "Rp 120.000", "https://cdn.pixabay.com/photo/2017/05/10/14/46/cat-2301015__480.jpg"),
        Transaction("Pembelian tiket pesawat", "Gagal", "Rp 2.500.000", "https://cdn.pixabay.com/photo/2016/02/19/10/05/cat-1209067__480.jpg"),
        Transaction("Pembayaran tagihan internet", "Berhasil", "Rp 500.000", "https://cdn.pixabay.com/photo/2017/05/13/09/47/cat-2309127__480.jpg")
    )
}